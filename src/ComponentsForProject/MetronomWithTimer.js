import React from 'react';

const metronomWithTimer = (props) => {
	return (
		<div className="main">
			<div className="countdown"> 
				<h3>Hello! I'm Timetronom and I count down seconds in a pace of 60 bps </h3>
				<h1 id="countdown">{isNaN(props.labelsec) ? 0 : props.labelsec} </h1>
			</div>
			<div className="div">
				<p id="numberOfSec"> Enter number of seconds: </p>
				<input id="inputSec" type="number" max="500" className="inputsec" onChange={props.inputHandler} value={props.inputsec} disabled={props.isMetronomRunning}/>
			</div>
			<div className="div">
				<button id="startButton" className="btn-lg" onClick={props.startButtonHandler} disabled={props.isMetronomRunning}> Start! </button>  
				<button id="stopButton" className="btn-lg" onClick={props.stopButtonHandler} disabled={!props.isMetronomRunning}> Stop </button>
				<button id="resetButton" className="btn-lg" onClick={props.resetButtonHandler} disabled={!props.isMetronomRunning && !props.isStopRequested}> Reset </button>
			</div>
		</div>
	)
};

export default metronomWithTimer;